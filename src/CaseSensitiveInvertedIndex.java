import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;


public class CaseSensitiveInvertedIndex extends AbstractInvertedIndex {
    private static CaseSensitiveInvertedIndex caseSensitiveIndexSingleton;

    private CaseSensitiveInvertedIndex(){
        invertedMap = new HashMap<String, TreeSet<String>>();
    }

    public static CaseSensitiveInvertedIndex getInstance(){
        if(caseSensitiveIndexSingleton == null){
            caseSensitiveIndexSingleton = new CaseSensitiveInvertedIndex();
            System.out.println("New CaseSensitive index is created");
        }
        else
            System.out.println("You already have a CaseSensitive index");
        return caseSensitiveIndexSingleton;
    }

    public void buildInvertedIndex (File[] listFiles) {
        List<String> fileLines;
        for(File file : listFiles){
            fileLines = Utils.readLines(file);
            for(String line : fileLines){
                String[] lineWords = Utils.splitBySpace(line);
                for(String word : lineWords){
                    String fileName = Utils.substringBetween(fileLines.get(1), "<DOCNO> ", " </DOCNO>");
                    if(!invertedMap.containsKey(word)) {
                        invertedMap.put(word, new TreeSet<String>());
                    }
                    invertedMap.get(word).add(fileName);
                }
            }
        }
    }
}
