import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

public class CaseInsensitiveInvertedIndex extends AbstractInvertedIndex{
    private static CaseInsensitiveInvertedIndex caseInsensitiveIndexSingleton;

    private CaseInsensitiveInvertedIndex(){
        invertedMap = new HashMap<String, TreeSet<String>>();
    }

    public static CaseInsensitiveInvertedIndex getInstance(){
        if(caseInsensitiveIndexSingleton == null){
            caseInsensitiveIndexSingleton = new CaseInsensitiveInvertedIndex();
            System.out.println("New CaseInsensitive index is created");
        }
        else
            System.out.println("You already have a CaseInsensitive index");
        return caseInsensitiveIndexSingleton;
    }

    public void buildInvertedIndex (File[] listFiles) {
        List<String> fileLines;
        for(File file : listFiles){
            fileLines = Utils.readLines(file);
            for(String line : fileLines){
                String[] lineWords = Utils.splitBySpace(line);
                for(String word : lineWords){
                    word = word.toLowerCase();
                    String fileName = Utils.substringBetween(fileLines.get(1), "<DOCNO> ", " </DOCNO>");
                    if(!invertedMap.containsKey(word)) {
                        invertedMap.put(word, new TreeSet<String>());
                    }
                    invertedMap.get(word).add(fileName);
                }
            }
        }
    }
}

