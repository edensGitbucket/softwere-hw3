import java.io.File;
import java.util.HashMap;
import java.util.Stack;
import java.util.TreeSet;

public abstract class AbstractInvertedIndex {
    HashMap<String, TreeSet<String>> invertedMap;
    public abstract void buildInvertedIndex(File[] listFiles);

    public TreeSet<String> runQuery(String query) {
        Stack<String> queryStack = new Stack<>();
        Stack<TreeSet<String>> stackTree = new Stack<>();
        String operators = "AND OR NOT";
        for(String word : Utils.splitBySpace(query)){
            if(!operators.contains(word)){
                if(this instanceof CaseInsensitiveInvertedIndex)
                    queryStack.push(word.toLowerCase());
                else
                    queryStack.push(word);
            }
            else{
                String secondWord = queryStack.pop();
                String firstWord = queryStack.pop();
                TreeSet<String> firstWordTree = new TreeSet<>();
                TreeSet<String> secondWordTree = new TreeSet<>();
                switch(word){
                    case "AND":
                        queryStack.push("ISATREE!");

                        if(!(secondWord.equals("ISATREE!")))
                            secondWordTree.addAll(invertedMap.get(secondWord));
                        else
                            secondWordTree = stackTree.pop();
                        if(!(firstWord.equals("ISATREE!")))
                            firstWordTree.addAll(invertedMap.get(firstWord));
                        else
                            firstWordTree = stackTree.pop();
                        TreeSet<String> tempTree = new TreeSet<>();
                        for(String file : firstWordTree)
                            if(secondWordTree.contains(file))
                                tempTree.add(file);
                        stackTree.push(tempTree);
                        break;
                    case "OR":
                        queryStack.push("ISATREE!");
                        if(!(secondWord.equals("ISATREE!")))
                            secondWordTree.addAll(invertedMap.get(secondWord));
                        else
                            secondWordTree = stackTree.pop();
                        if(!(firstWord.equals("ISATREE!")))
                            firstWordTree.addAll(invertedMap.get(firstWord));
                        else
                            firstWordTree = stackTree.pop();
                        firstWordTree.addAll(secondWordTree);
                        stackTree.push(firstWordTree);
                        break;
                    case "NOT":
                        queryStack.push("ISATREE!");
                        queryStack.push("ISATREE!");
                        if(!(secondWord.equals("ISATREE!")))
                            secondWordTree.addAll(invertedMap.get(secondWord));
                        else
                            secondWordTree = stackTree.pop();
                        if(!(firstWord.equals("ISATREE!")))
                            firstWordTree.addAll(invertedMap.get(firstWord));
                        else
                            firstWordTree = stackTree.pop();
                        for(String file : secondWordTree)
                            firstWordTree.remove(file);
                        stackTree.push(firstWordTree);
                        break;
                    default:
                        System.out.println("Please enter a valid query");
                        System.exit(1);
                }
            }
        }
        if(!stackTree.isEmpty())
            return stackTree.pop();
        else
            return invertedMap.get(queryStack.pop());
    }
}